/*
Arrays - used to store multiple related values  in a
single variable.
- declared using square brackets[] known as arrays literals
Elements - are values inside an array.

index - 0 = offset (0, 1, 2, 3 .. . . )
element 1 = index 0 and so on...

Syntax: let/const arrayName = [elementA, elementB, elementC]
*/

//example#1
let grades = [98, 94, 89, 90];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo',
	'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//Not Recommended format
let mixedArr = [12, 'Asus', null, undefined, {}]; //not recommended need related values 
console.log(mixedArr);

//Reading
console.log(grades[0]); //98
console.log(computerBrands[3]); //Neo
console.log(grades [10]); // undefined

//example #2
let myTask = [
	'bake sass',
	'drink html',
	'inhale css',
	'eat javascript'
];
console.log(myTask);

//Reassinging value
myTask[0] = 'hello world';
console.log(myTask);

//Getting the lenght of an array - counting the item inside array
console.log(computerBrands.length);

//Getting length of element(value)
// let lastIndex = myTask.length - 1 //output: 3 - eat javascript is last index
// console.log(lastIndex);

/*
ARRAY METHODS
1. Mutator Methods
-are functions that "mutate" or change an array
*/
//push
//-adds an element in the end of the array and return its length
//syntax: arrayName.push(elementA, elementB)
//example
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log('Current array');
console.log(fruits);
console.log('Array after push()');

//adding and pushing mango
let fruitsLenght = fruits.push('Mango');
console.log('Mutated array after push()')
console.log(fruits);
console.log(fruitsLenght);

//adding and pushing langka
fruits.push('langka');
console.log(fruits);

//removing an element in array
//pop - remove the last element of an array
let removedFruit = fruits.pop();
console.log('Mutated array after pop()')
console.log(fruits);
console.log(removedFruit);

//shift - removes an element at the beginning of an array and return the removed element 
let firstfruits = fruits.shift();
console.log('Mutated array after shift()')
console.log(fruits);
console.log(firstfruits);

//unshift = adds one or more element/s at the beginning of an array
fruits.unshift('Lime', 'Banna');
console.log(fruits);

//splice - removes element from specified index and adds new elements
//syntax; arrayName.splice(startingIndex, deleteCount, elementes to be added)
console.log('Mutated array after splice()')
fruits.splice(1, 2, 'Cherry', 'Grapes');
console.log(fruits);

//sort
fruits.sort();
console.log('Mutated array after sort()')
console.log(fruits);

//reverse - reverse the order of array (descening and ascending - vise versa)
fruits.reverse();
console.log('Mutated array after sort()')
console.log(fruits);

/*
2. Non Mutator Methods
- are functions that do not modify or change the array
*/
//example
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR',
	'DE'];
//indexof() - returns the first index of the first matching element found in an array
//if no match is found, it returns -1
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf() method:' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf() method:' + invalidCountry);

//lastIndexof() - returns the last matching element
//syntax: arrayName.lastIndexOf(searchValue);
//arrayName.lastIndexOf(searchValue, fromIndex);
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf() method:' + lastIndex);

let lasttIndexStart = countries.lastIndexOf('PH', 7); //output is 5 last index 7 and it count backwards
console.log('Result of lastIndexOf() method:' + lasttIndexStart);

//slice - slices a portion of an array and return a new array
//syntax: arrayName.slice(startingIndex);
//syntax: arrayName.slice(startingIndex, endingIndex);
let slicedArrayA = countries.slice(2);
console.log('Result from slice method A');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method B');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method C');
console.log(slicedArrayC);
console.log(countries);

//toString
let stringArray = countries.toString();
console.log('Result from toString');
console.log(stringArray);

//concatenate - combines two or more arrays and return the combined result
let tasksArrayA = ['drink HTML', 'eat javascript'];
let tasksArrayB = ['inhale CSS', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];
let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat()');
console.log(tasks);

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat()');
console.log(allTasks);

let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat()');
console.log(combinedTasks);

//join - returns an array as string separated by specified separator
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' & '));

/*



3. Iteration Methods
-loops designed to perfor repetitive tasks on arrays
*/
/*
//foreach
//sytax:
//arrayName.forEach(function(indvidualElement){
	statment
})
*/

countries.forEach(function(country){ // sorts through each element arraylist
	console.log(country);
})

allTasks.forEach(function(task){
	console.log(task);
})

let filteredTasks = [];
allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})
console.log('Result from forEach()');
console.log(filteredTasks);

//map - iterates on each element and returns new array with different values depending on the results of the function's operation
let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number;
})
console.log('Result from map()');
console.log(numberMap);

//forEach - map
let numberMap2 = [];
numbers.forEach(function(number){
	let square = number * number;
	numberMap2.push(square);
})
console.log('Result from forEach()');
console.log(numberMap2);

//Map thru String
let arrayMap = allTasks.map(function(task){
	return `I ${task}`;
})
console.log('Result from arrayMap()')
console.log(arrayMap);

//every
let allValid = numbers.every(function(number){
	return (number < 3)
})
console.log('Result from every()');
console.log(allValid);

//some
let someValid = numbers.some(function(number){
	return (number < 3)
})
console.log('Result from some()');
console.log(someValid);

//filter - returns a new array that meets the given condition
let filterValid = numbers.filter(function(number){
	return (number < 3)
})
console.log('Result from filter()');
console.log(filterValid);

//includdes
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let filterProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
})
console.log('Result from filter() & includes');
console.log(filterProducts);

//reduce
/*syntax: let/const resultArray = arrayName.reduce(fucntion(accumulator, currentValue){
	statement
})
*/
//example


//Multi-dimentional Array - Reading from (Row and column)
let chessBoard = [
	//0    //1   //2   //3   //4   //5   //6   //7
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'], //0
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'], //1
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'], //2
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'], //3
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'], //4
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'], //5
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'], //6
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'] //7
];
console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log('Pawn moves to f2: ' + chessBoard [1] [5]);
console.log('Knight moves to c7: ' + chessBoard [6] [2]);


